<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859" />
	<meta name="language" content="es" />

	<!-- blueprint CSS framework -->
	<?php echo WebApp::useScript('screen.css',"media='screen, projection'"); ?>
	<?php echo WebApp::useScript('print.css',"media='print'"); ?>
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<?php echo WebApp::useScript('main.css'); ?>
	<?php echo WebApp::useScript('form.css'); ?>

	<title>Yii Framework en Espa�ol</title>
</head>

<body>

<div class="container" id="page">

	<div id="header" >
		<div id="logo">
			<a href='index.php?r=site/index'><?php echo WebApp::getLogo(); ?></a>
			<span class='slogan'><?php echo WebApp::getSlogan(); ?></span>
		</div>
		<div id='header-right'>
			<?php echo WebApp::accountPanel(); ?><br/>
			<?php WebApp::getMbMenu($this); ?>
		</div>
	</div><!-- header -->

	<?php echo $content; ?>
</div><!-- page -->

<?php echo WebApp::footer(); ?>

</body>
</html>