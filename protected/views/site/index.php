<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=322320361187352";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<h1>Estamos construyendo el sitio web</h1>

<style>
	#mensaje1{
		color: black;
		background-color: gold;
		text-align: center;
		border-radius: 5px;
		padding: 3px;
	}
	#testlist{
		list-style: none;
	}
	#testlist li{
		margin-bottom: 10px;
	}
	#testlist li a{
		color: blue;
	}
	#foro a{
		padding: 3px;
		border-radius: 5px;
	}
	div.fb-comments,
	div.fb-like
	{
		float: right;
		width: auto;
	}
	div.contenido{
		float: left;
		width: 60%;
	}
	div.pagina{
		overflow: auto;
		padding: 0px;
		margin-top: 30px;
	}
	ul.socialicons{
		margin: 0px;
		padding: 0px;
		list-style: none;
	}
	ul.socialicons li{
		float: left;
		margin-right: 10px;
		margin-bottom: 10px;
	}
</style>

<div class='pagina'>
	<div class='contenido'>
		<ul id='testlist'>
			<li id='foro'><a href='../foro'>Ingresa al Foro</a></li>
			<li id='foro'>
				<?php echo CHtml::link("Chat en Vivo",array('site/page','view'=>'chat'));?>
			</li>
			
			<li id='foro'><a href='index.php?r=site/ejemplos'>Lista de Ejemplos</a></li>
			<li><a href='https://github.com/yiiframeworkenespanol/ejemplos'>Repositorio de Ejemplos Yii en Github</a></li>
			<li><a href='index.php?r=site/contact'>Cont�ctanos</a></li>
			<li><a href='https://www.github.com/yiiframeworkenespanol'>Github para Proyectos</a></li>
		</ul>
		<ul class='socialicons'>
			<li><a href='https://www.facebook.com/groups/171245476282742/'>
				<img src='css/bs-facebook.png'></a></li>
			<li><a href='https://twitter.com/#!/yiienespanol'>
				<img src='css/bs-twitter.png'></a></li>
		</ul>
		
		<p><h2>Bienvenido a Yii Framework en Espa�ol !</h2>Esta es una inciativa para unirnos mientras el trabajo se va realizando, si es cierto, no esta lista y falta mucho por completar pero decidimos lanzar este trabajo para que te unas mientras vamos pensando !, hay proyectos interesantes en construcci�n, si tienes una idea discutelo ac�, si quieres contactarnos busca el enlace arriba.	
		<br/><br/>
		<p>Por mientras, usa el foro para ayudar y ser ayudado, la comunidad te espera !</p></p>
		
<a href="https://twitter.com/yiienespanol" class="twitter-follow-button" data-show-count="false" data-lang="es">Seguir a @yiienespanol</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		
	</div>
	<div class="fb-comments" data-href="http://www.yiiframeworkenespanol.org" data-num-posts="10" data-width="300px"></div>
	<div class="fb-like" data-href="http://www.yiiframeworkenespanol.org" data-send="true" data-width="200" data-show-faces="true"></div>	
</div>

