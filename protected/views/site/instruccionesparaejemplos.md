Normas para recibir tu Ejemplo
------------------------------

Para insertar un "ejemplo" debes cumplir con algo m�nimo:

1. **Tu ejemplo debe venir en la forma de "modulo"** y debe tener un nombre �nico y corto, [ver nombres en uso](index.php?r=site/listamodulos). Un esqueleto de modulo se crea con la herramienta Gii, toda lafuncionalidad requerida de tu ejemplo debe caber en el modulo.

2. **Agrega un archivo'README.md' en la raiz del modulo**, con la siguiente estructura:
	1. `##TITULO DEL MODULO`
	2. `####AUTOR-CORREO`
	3. `####CATEGORIA, ejemplo: how-to, tutorial`
	4. `..resto del archivo debe ser la descripcion detallada de tu ejemplo, en formato MD (markdown)..`
	
4. **La base de datos debe ser SQLite**, a continuaci�n puedes leer con detalle como usar sqlite.

6. [Publica tu modulo en el Repositorio Oficial de Yii Framework en Espa�ol en Github](https://www.github.com/yiiframeworkenespanol), para ello
deber�s escribirnos a **contacto@yiiframeworkenespanol.org** para solicitar acceso al repositorio.
	
	**PARA QUE TU EJEMPLO SE PUBLIQUE DEBERAS NOTIFICAR POR CORREO, ES POR AHORA, MAS ADELANTE SE HARA DE FORMA AUTOMATICA, TE PEDIMOS DISCULPAS: contacto@yiiframeworkenespanol.org**
	
---	
	
Usando SQLite
-------------

Todos los demos deben usar una base de datos SQLITE en caso de requerir datos. Para usar sqlite debes descargar un peque�o cliente para gestionar las tablas y tu propia base de datos, para descargar el cliente sqlite entra aqui: <a href='http://www.sqlite.org/'>http://www.sqlite.org/</a>

Debes configurar la extension en tu PHP.INI: extension=php_pdo_sqlite.dll y extension=php_sqlite.dll

Para el caso de Windows, puedes descargar este peque�o EXE:
[cliente sqlite para windows](http://www.sqlite.org/sqlite-shell-win32-x86-3071300.zip)

Es muy simple usar SQLITE, simplemente en la carpeta /protected/data usa tu shell prompt para ir hacia 
el directorio:
/e/code/yiiframeworkenespanol/ejemplos/protected/data
Luego, ejecutas sqlite3.exe seguido del nombre de la base de datos de tu demo.

	publico@COCO /e/code/yiiframeworkenespanol/ejemplos/protected/data (master)
	$ sqlite demo1.db
	SQLite version 3.7.13 2012-06-11 02:05:22
	Enter ".help" for instructions
	Enter SQL statements terminated with a ";"
	sqlite>

luego, crear tablas para el demo:

	sqlite> create table categoria(idcategoria integer, nombre char(45));
	sqlite> create table producto(idproducto integer,idcategoria integer, nombre char(45), precio float);

insertar algunos datos de ejemplo:

	sqlite> insert into categoria values(1,'vehiculos');
	sqlite> insert into categoria values(2,'ropa');
	sqlite> insert into categoria values(3,'alimentos');

	sqlite> insert into producto values(1,1,'chevrolet aveo',125000);
	sqlite> insert into producto values(2,1,'ford fiesta',120000);
	sqlite> insert into producto values(3,1,'malibu classic',80000);
	sqlite> insert into producto values(4,2,'camisas',1000);
	sqlite> insert into producto values(5,2,'pantalones',1200);
	sqlite> insert into producto values(6,3,'cereales',80);
	sqlite> insert into producto values(7,3,'granos',25);

consultas:

	sqlite> select * from categoria;
	1|vehiculos
	2|ropa
	3|alimentos

	sqlite> select * from producto;
	1|1|chevrolet aveo|125000.0
	2|1|ford fiesta|120000.0
	3|1|malibu classic|80000.0
	4|2|camisas|1000.0
	5|2|pantalones|1200.0
	6|3|cereales|80.0
	7|3|granos|25.0

---	
	
