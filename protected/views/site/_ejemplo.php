<div id='ejemplo-fila'>
<h1><?php echo Yii::app()->ejemplos->getDirectLink($data);?></h1>
<h6>Categor&iacute;a: <b><?php echo $data->categoria;?></b></h6>
<h6>Autor: <b><?php echo $data->autor;?></b></h6>
<hr/>
<div class='vista'>
	<div class='activador'>Mostrar / Ocultar Descripcion</div>
	<div class='detalles'><?php echo $data->descripcion;?></div>
</div>
<br/>
<ul class='listalinks'>
	<li><?php echo Yii::app()->ejemplos->getUrlModulo("Ejecutar el Ejemplo",$data);?></li>
	<li><?php echo Yii::app()->ejemplos->getUrlGithub("Ir al C�digo (github)",$data);?></li>
</ul>
</div>