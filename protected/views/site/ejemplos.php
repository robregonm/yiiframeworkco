<h1>Ejemplos</h1>

<p>Estimado miembro de la comunidad, esta es una lista de ejemplos bien probados y soportados por
al menos Yii 1.X, si deseas incorporar un nuevo ejemplo hecho por ti deberas <a href='index.php?r=site/instruccionesparaejemplos'>conocer las normas para crear ejemplos</a>.</p>

<style>
	#ejemplos {
		border-radius: 5px;
	}
	#ejemplos h1 {
		font-size: 20px;
	}
	#ejemplos h6 {
		font-weight: normal;
		float: left;
		margin-right: 20px;
	}
	#ejemplo-fila{
		background-color: #efefef;
		padding: 10px;
		display: block;
		margin-bottom: 20px;
	}
	ul.listalinks{
		overflow: auto;
		text-align: center;
		list-style: none;
	}
	ul.listalinks li{
		float: left;
		margin-right: 50px;
		background-color: rgb(255,250,200);
		padding: 3px;
		border-radius: 5px;
	}
	
	#ejemplos .vista{
		margin: 20px;
	}
	#ejemplos .vista .activador{
		cursor: pointer;
		color: blue;
	}
	#ejemplos .vista .detalles{
		<?php if(!isset($_GET['modulo'])) echo "display: none;";?>
	}
	
</style>

<div id='ejemplos'>
<?php 
	$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_ejemplo',
		'sortableAttributes'=>array(
			'nombre',
			'autor',
			'categoria',
		),
	));
?>
</div>


<script>
	$('.activador').click(function(){
		$(this).parent().find('.detalles').toggle();
	});
</script>



<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33386499-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
