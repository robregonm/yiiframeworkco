<?php 
	/**	Helper class for theme and web app managment.
	
		@author:  Christian Salazar  https://github.com/christiansalazar  christiansalazarh@gmail.com  @bluyell
	*/
	class WebApp {
	
		public static function footer(){
?>
	<div id='footer' >
		<div id='footer-inner'>
			<div class='footer-left'>
				
				<?php
					$ar = self::getMenuItems();
					foreach($ar as $menu){
						echo "<div class='foot-menu-entry'><ul class='footer-menu-ul'>";
						echo "<li class='li-head'><a href='".$menu['url']."'>".$menu['label']."</a></li>";
						if(isset($menu['items'])){
							echo "<ul class='sublista'>";
							foreach($menu['items'] as $submenu){
								echo "<li><a href='".$submenu['url']."'>".$submenu['label']."</a></li>";
							}
							echo "</ul>";
						}
						echo "</ul></div>";
					}
				?>
				<!--
				<div class='foot-menu-entry'><ul class='footer-menu-ul'>
					<li class='li-head'><a href='#'>Bienvenido</a></li>
				<?php
					$ar = self::topMenuItems();
					foreach($ar as $menu){
						echo "<li><a href='".$menu['url']."'>".$menu['label']."</a></li>";
					}
				?>
				</ul></div>
				-->
			</div>
		</div>
	</div>
	<div id='sub-footer' >
		<div id='footer-inner'>
			<div class='footer-left'>
				<img src='themes/yii-theme/css/yii-la--bottom.png' />
			</div>
			<div class='footer-right'>
				La comunidad de <b>Yii Framework en Espa�ol</b> construye y mantiene este sitio web bajo el soporte de: <a href='http://www.yiiframework.com'>Yii Framework</a>.
			</div>
		</div>
	</div>
<?php
			}
	
		
		public static function topMenuItems(){
			return array(
				array('label'=>'Acceder'
					,'url'=>'http://www.yiiframeworkenespanol.org/foro/member.php?action=login'),
				array('label'=>'Registrarse'
					,'url'=>'http://www.yiiframeworkenespanol.org/foro/member.php?action=register'),
				array('label'=>'Contactanos','url'=>'index.php?r=site/contact'),
				array('label'=>'Inicio','url'=>'index.php?r=site/index'),
			);
		}
	
		public static function accountPanel()
		{
			$menues = self::topMenuItems();
			$ht = "<ul id='top-menu-items'>";
			$last="";
			$n=0;
			foreach($menues as $m){
				if($n == (count($menues)-1))
					$last = "class='last'";
				$item = "<a href='".$m['url']."'><span>".$m['label']."</span></a>";
				$ht .= "<li ".$last.">".$item."</li>";
				$n++;
			}
			$ht .= "</ul>";
			return $ht;
			/*
			if(Yii::app()->user->isGuest){
				return CHtml::link("Accede a tu Cuenta",array('site/login'));
			}else{
				$name = Yii::app()->user->name;
				return "<ul><li>Bienvenido a YiiFramework Latinoam�rica ! <span class='loginname'>{$name}</span></li><li>".CHtml::link("Exit",array('site/logout'))."</li></ul>";
			}
			*/
		}
		
		/** returns the base path for theme resources
			
		*/
		public static function getThemeFolder(){
			return "themes/".Yii::app()->theme->name."/";
		}
		
		
		
		/** creates a script path relative to theme folder
			@example:
			<?php echo WebApp::useScript('print.css',"media='print'"); ?>
			will output:
			<link rel='stylesheet' type='text/css' href='themes/system-office-1/css/print.css' media='print' />
		*/
		public static function useScript($filename,$extra=""){
			$path = self::getThemeFolder()."css/".$filename;
			
			// determine file extension
			$extension = strrev(substr(strrev(trim($filename)),0,3));
			
			if($extension == 'css')	// stands for CSs
			  return "<link rel='stylesheet' type='text/css' href='{$path}' {$extra} />\n";
			if($extension == '.js')	
			  return "<script src='{$path}' {$extra} ></script>\n";
			  
			 return "\n<!-- error in file reference for: {$filename} , extension: {$extension}-->\n";
		}
		
		public static function getLogo(){
			return CHtml::image(self::getLogoFileName());
		}
		public static function getLogoFileName(){
			return self::getThemeFolder()."/css/logo.png";
		}
		
		public static function getSlogan(){
			return "Tu Comunidad de Yii Framework en Espa�ol";
		}
		
		
		public static function getMbMenu($controllerInstance){
			$controllerInstance->widget('application.extensions.mbmenu.MbMenu'
				,array(
					'themeSources'=>self::getThemeFolder().'/extras/mbmenu-sources/',
					'iconpack'=>self::getThemeFolder()."/generic-icon-pack-16x16.png",
					'items'=>self::getMenuItems()
				)
			);			
		}
		
		
		/** return the menu items for the main menu.
			
			at this point you can filter wich items should be shown to final user, depending on it user level.
		*/
		public static function getMenuItems(){
		
			/*
				// as an example, if user is not admin or whatever, return this specific menu:
				// you can now perform menu filtering
				//
			if(...any condition..){
				return array(
					array('label'=>'Another page', 'url'=>array('/site/index')),
				);
			}
			else
				...another menu...
				
				
				
				HOW ICON WORKS ?
				----------------
				
				Please note:
				array('label'=>'Home', 'url'=>array('/site/index'),'icon'=>'-377px -234px'),
				
				and take a deep look at:
				'icon'=>'-377px -234px'
				
				the 'icon' attribute, if present, define the icon position on a global file
				predefined when you setup your MBMenu,  this is most like stablished on WebApp.php::getMbMenu
				in this method (getMbMenu) you point your mbmenu component to graphic file (jpg, gpf, png) 
				containing a lot of icons (the iconpack)
				
				next, with this in mind, open the main iconpack file using your preferred image editor 
				and get the icon position for your preferred icon:
				
				take care of:
				
				if your icon is at 321px in X, and in 283px in Y, then you must specify this coordinates
				for your icon using negative values:  'icon'=>'-321px -283px'
			*/
		
			return 
			array(
				array(
						'label'=>'Acerca', 
						'url'=>array('/site/index'),
						'items'=>array(
							array('label'=>'Yii Latinoamerica','url'=>'#1'),
							array('label'=>'Framework','url'=>'#1'),
							array('label'=>'Performance','url'=>'#1'),
							array('label'=>'Licencia','url'=>'#1'),
						),
					),
				array('label'=>'Descargas','icon'=>'-101px -487px','url'=>array('/site/page', 'view'=>'system'),
						'items'=>array(
							array('label'=>'Descarga Yii','url'=>'#1'),
							array('label'=>'Extensiones y Componentes','url'=>'#'),
							array('label'=>'Logos','url'=>'#'),
						),
					),
				array('label'=>'Docs','icon'=>'-72px -359px', 'url'=>array('/site/page', 'view'=>'operations'),
						'items'=>array(
							array('label'=>'Iniciarse en Yii','url'=>'#4'),
							array('label'=>'Ejemplos','url'=>array('/site/ejemplos')),
							array('label'=>'Tutoriales','url'=>'#5'),
							array('label'=>'Referencia de Clases','url'=>'#6'),
							array('label'=>'Nuestra Wiki','url'=>'#6'),
						),
					),
				array('label'=>'Audiovisual','icon'=>'-72px -359px', 'url'=>array('/site/page', 'view'=>'operations'),
						'items'=>array(
							array('label'=>'Yii-La en Youtube','url'=>'#4'),
							array('label'=>'Envia tu video','url'=>'#5'),
							array('label'=>'Videos por Categoria','url'=>'#6'),
						),
					),
				array('label'=>'Desarrollo', 'icon'=>'-266px -455px', 'url'=>array('/site/contact'),
						'items'=>array(
							array('label'=>'Contribuir','url'=>'#4'),
							array('label'=>'Crea tu Ejemplo','url'=>array('site/instruccionesparaejemplos')),
							
							array('label'=>'Organizacion','url'=>'#5'),
							array('label'=>'Reporta un Bug','url'=>'#6'),
							array('label'=>'Reporta un Bug de Seguridad','url'=>'#6'),
						),
					),
				array('label'=>'Comunidad', 'icon'=>'-377px -202px','url'=>array('/site/page', 'view'=>'about'),
						'items'=>array(
							array('label'=>'Foro','url'=>'../foro'),
							array('label'=>'Chat (#yiienespanol)'
								,'url'=>array('site/page','view'=>'chat')),
							array('label'=>'Noticias','url'=>'#6'),
							array('label'=>'Ayudanos a Traducir','url'=>'#6'),
						),
					),
				
			);		
		}
		
		
		
		public static function getBannerFilename($themeRelativeBannerFileName){
			return self::getThemeFolder()."css/".$themeRelativeBannerFileName;
		}
		
		public static function displayBanner($fullBannerPath){
			return 
"<script>
	$('#main-banner').css('background-image',\"url(\'".$fullBannerPath."\')\");
	$('#main-banner').show();
</script>";
		}
		
	}
?>