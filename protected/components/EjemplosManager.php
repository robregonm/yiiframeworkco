<?php 
	/*	EjemplosManager
	
		obtiene datos sobre los ejemplos instalados
	
		@author: Christian Salazar H. <christiansalazarh@gmail.com> @bluyell
	*/
	class EjemplosManager extends CApplicationComponent {
	
		private $_lista = null;
		public $urlBaseModulos;
		public $rutaLocalModulos;
		public $urlBaseGithub='https://github.com/yiiframeworkenespanol/ejemplos/tree/master/protected/modules/';
	
		public $urlBase = 'http://www.yiiframeworkenespanol.org/';
	
		public function init(){
			Yii::log("EjemplosManager inicializado","info");
		}
	
		/*
			@returns Array de objetos de clase Ejemplo leidos del repositorio de modulos de ejemplo
		*/
		public function getListado($modulo=""){
			Yii::log("EjemplosManager.getListado","info");
			if($this->_lista == null){
				$this->_lista = array();	
				
				/*
				$e = new Ejemplo();
				$e->titulo = 'Como hacer un maestro-detalle usando CGridView y Ajax';
				$e->descripcion = 'Ejemplo de maestro detalle usando Ajax y CGridView';
				$e->autor = 'Christian Salazar H. <christiansalazarh@gmail.com> @bluyell';
				$e->modulo = 'demo1';
				$e->categoria = 'how-to';
				*/
				
				if ($dh = opendir($this->rutaLocalModulos)) {
					while (($file = readdir($dh)) !== false) {
						if(!is_dir($file)){
							$e = $this->newFromFile($file,$this->rutaLocalModulos."/".$file."/README.md");
							if($e != null){
								if($modulo != ''){
									if($modulo == $e->modulo)
										$this->_lista[] = $e;
								}else{
									$this->_lista[] = $e;
								}
							}
						}
					}					
				}
				
			}
			return $this->_lista;
		}
		
		
		public function getDataProvider($modulo=""){
			return new CArrayDataProvider($this->getListado($modulo), array(
				'id'=>'ejemplos-dataprovider',
				'sort'=>array(
					'attributes'=>array(
						 'titulo', 'autor', 'categoria',
					),
				),
				'pagination'=>array(
					'pageSize'=>10,
				),
			));				
		}
		
		public function getUrlModulo($titulo,Ejemplo $model){
			$url = $this->urlBaseModulos."/index.php?r=/".$model->modulo;
			return CHtml::link($titulo,$url,array('target'=>'_blank'));
		}
		public function getUrlGithub($titulo,Ejemplo $model){
			$url = $this->urlBaseGithub.$model->modulo;
			return CHtml::link($titulo,$url,array('target'=>'_blank'));
		}
		
		public function getDirectLink(Ejemplo $model){
			$url = $this->urlBase."index.php?r=site/ejemplos&modulo=".$model->modulo;
			return CHtml::link($model->titulo,$url,array('target'=>'_blank'));
		}
		
		public function newFromFile($moduloName,$file){
			
			$f = fopen($file,"rt");
			if($f != null){
				$e = null;
				$linea1 = ltrim(@fgets($f,250),'#');
				$linea2 = ltrim(@fgets($f,250),'#');
				$linea3 = ltrim(@fgets($f,50),'#');
				$e = new Ejemplo();
				$e->modulo = $moduloName;
				$e->titulo = $linea1;
				$e->autor = $linea2;
				$e->categoria = $linea3;
				$e->descripcion = MarkdownViewer::getText(fread($f,8192));
				fclose($f);
				return $e;
			}else
			return null;
		}
	}
?>