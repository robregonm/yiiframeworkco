<?php
	require_once('protected/components/markdown.php');
	/**  Wrapper para markdown.php
	
		@author: Christian Salazar H. <christiansalazarh@gmail.com> @bluyell
	*/
	class MarkdownViewer {
	
		public static function getText($text){
			return Markdown($text);
		}
		
		public static function getFromFile($filePath){
			return MarkDown(file_get_contents($filePath));
		}
	}
?>