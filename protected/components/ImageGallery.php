<?php
/** Create an image gallery using only HTML and CSS.

		Simple Usage:
		
		echo ImageGallery::read("images/partners","simplest");
		
		"image/partners" is a sample directory, containing a set of images,
		and "simplest" is a style name used to render it.  
		
		You can create your own styles in the array returned by getStyles() method.
	
	@author:  Christian Salazar christiansalazarh@gmail.com
*/
class ImageGallery {

	
	private static $prefix = 'imagegallery-';
	
	/** display a photo gallery.
	
		@example
		
		Usage:
		
		echo ImageGallery::read("images/partners","simplest");
		
	
		@param $imagesDirectory a image directory, example: "images/myphotos"
		@param $useStyleName a predefined style name, see also: getStyles, it must match an key array defined here.
		@return HTML TAG, example: <DIV><img>...<img>...</DIV>
	*/
	public static function read($imagesDirectory,$useStyleName=null){
		$classtag = "";
		$r="";
		
		$styles = self::getStyles();
		$prefix = self::$prefix;
		
		if($useStyleName != null){
			if(isset($styles[$useStyleName])){
				$r .= $styles[$useStyleName];
				$classtag = "class='{$prefix}{$useStyleName}'";
			}
		}
		$r .= "<div {$classtag}>";
		foreach(scandir($imagesDirectory) as $f)
			if(!is_dir($f))
				$r .= "<img src='{$imagesDirectory}/{$f}'>";
		$r .= "</div>";
		return $r;
	}

	/**  Availabe style names:	"simplest"
	*/
	private static function getStyles(){
		return array(
		"simplest"=>
			"<style>
				div.imagegallery-simplest{
					margin: 0px;
					padding: 0px;
					overflow: auto;
					text-align: center;
				}
				div.imagegallery-simplest img{
					max-width: 150px;
					margin: 30px;
				}
			</style>",
		);
	}
}