<?php 
	/* Clase que presenta un Ejemplo para la lista de ejemplos del site.
	
	
		@author: Christian Salazar H. <christiansalazarh@gmail.com> @bluyell
	*/
	class Ejemplo {
		public $titulo;
		public $descripcion;
		public $autor;
		public $modulo;
		public $categoria;
	}
?>